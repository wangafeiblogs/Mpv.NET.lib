﻿using Mpv.NET.Player;
using System.Windows.Forms;

namespace Mpv.NET.WinFormsExample
{
    public partial class MainForm : Form
    {
        private MpvPlayer player;

        public MainForm()
        {
            InitializeComponent();

            player = new MpvPlayer(this.Handle)
            {
                Loop = true,
                Volume = 50
            };
            player.Load(@"C:\Livewallpaper\0f29e29d-611f-4253-8b59-3f0c89ffe4dc\index.mp4");
            player.Load(@"C:\Livewallpaper\0f29e29d-611f-4253-8b59-3f0c89ffe4dc\index.mp4");
            player.Stop();
            player.Dispose();
            //player.Resume();
        }

        private void MainFormOnFormClosing(object sender, FormClosingEventArgs e)
        {
            player.Stop();
            player.Dispose();
        }
    }
}